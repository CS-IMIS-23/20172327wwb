package chapter4.pp4_2;

public class EmptyCollectionException extends RuntimeException {
    public EmptyCollectionException(String collection){
        super("The " + collection +" is empty.");
    }
}
