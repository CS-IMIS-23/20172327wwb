package 哈夫曼树编码实验;

public class Objects implements Comparable<Objects> {
    private String name;
    private double weight;
    private String date;
    private Objects left;
    private Objects right;
    public Objects(String Name , double Weight){
        name=Name;
        weight=Weight;
        date="";
    }
    public String getName() {
        return name;
    }

    public double getWeight() {
        return weight;
    }

    public Objects getLeft() {
        return left;
    }

    public Objects getRight() {
        return right;
    }

    public void setLeft(Objects left) {
        this.left = left;
    }

    public void setRight(Objects right) {
        this.right = right;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    @Override
    public String toString() {
        return "Objects{" + "name='" + name + '\'' + ", weight=" + weight + ", 编码为='" + date + '\'' + '}'+"\n";
    }

    @Override
    public int compareTo(Objects o) {
        if (weight>=o.weight){
            return 1;
        }
        else {
            return -1;
        }
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDate() {
        return date;
    }
}
