
//***********************************************
//
//      Filename: pp38.java
//
//        Author: wwb
//        Mails: 1274860621@qq.com
//        Create: 2018-03-21 15:40:40
// Last Modified: 2018-03-21 15:40:40
//***********************************************
      import java.util.Random;
     public class pp38
   {
	public static void main(String[]args)
   {
      Random generator = new Random();
     int num1;
     double num2, num3, num4;
  	num1 = generator.nextInt(20)+20;
     
     num2 = Math.cos(num1);
     num3 = Math.sin(num1);
     num4 = Math.tan(num1);
    
     System.out.println("cos: " + num2);
     System.out.println("sin: " + num3);
     System.out.println("tan: " + num4);
     System.out.println("num1: " + num1);
    }
} 
