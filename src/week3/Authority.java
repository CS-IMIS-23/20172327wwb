
//***********************************************
//
//      Filename: Authority.java
//
//        Author: wwb
//        Mails: 1274860621@qq.com
//        Create: 2018-03-26 20:58:04
// Last Modified: 2018-03-26 20:58:04
//***********************************************

import java.awt.*;
import javax.swing.*;
public class Authority {
    public static void main(String[] args) {
        JFrame frame = new JFrame("Authority");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        JFrame primary = new JFrame();
        primary.setBackground(Color.yellow);
        primary.setPreferredSize(new Dimension(250,75));
        JFrame label1 = new JFrame("Question authority,");
        JFrame label2 = new JFrame("but raise your hand first.");
        primary.add(label1);
        primary.add(label2);
        frame.getContentPane().add(primary);
        frame.pack();
        frame.setVisible(true);
    }
}
