     
//***********************************************
//
//      Filename: ex35.java
//
//        Author: wwb
//        Mails: 1274860621@qq.com
//        Create: 2018-03-20 21:19:36
// Last Modified: 2018-03-20 21:19:36
//***********************************************

public class ex35
     {
      public static void main(String[]args)
     {
        String m1, m2, m3;
        m1 = " Quest for the Holy Grail";
        m2 =  m1.toLowerCase();
        m3 = m1 + " " + m2;
        System.out.println(m3.replace('g', 'z'));
    }
    }  
 
