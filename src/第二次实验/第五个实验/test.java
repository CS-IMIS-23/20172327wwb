package 第二次实验.第五个实验;

public class test {
    public static void main(String[] args) {
        LinkedBinarySearchTree a =new LinkedBinarySearchTree();

        a.addElement(4);
        a.addElement(5);
         a.addElement(-1);
        a.addElement(1);
        a.addElement(2);
        a.addElement(3);
        System.out.println("打印树：");
        System.out.println(a.toString());
        System.out.println("寻找树中的最大值");
        System.out.println(a.findMax());
        System.out.println("寻找树中的最小值");
        System.out.println(a.findMin());
        System.out.println("删除树中最大值"+a.removeMax());
        System.out.println("删除后的树");
        System.out.println(a.toString());
        System.out.println("删除树中的最小值："+a.removeMin());
        System.out.println("打印树");
        System.out.println(a.toString());

    }
}
