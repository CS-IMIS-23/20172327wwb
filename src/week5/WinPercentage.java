
//***********************************************
//
//      Filename: WinPercentage.java
//
//        Author: wwb
//        Mails: 1274860621@qq.com
//        Create: 2018-04-07 20:19:49
// Last Modified: 2018-04-07 20:19:49
//***********************************************
import java.text.NumberFormat;
import java.util.Scanner;
public class WinPercentage
{
public static void main(String[]args)
{
final int NUM_GAMES = 12;
int won;
double ratio;
Scanner scan = new Scanner(System.in);
System.out.print("Enter the number of games won (0 to " + NUM_GAMES + ");");
won = scan.nextInt();
while (won < 0 || won > NUM_GAMES)
{
System.out.print("Invalid input. Please reenter: ");
won = scan.nextInt();
}
ratio = (double)won / NUM_GAMES;
NumberFormat fmt = NumberFormat.getPercentInstance();

System.out.println();
System.out.println("Winning percentage: " + fmt.format(ratio));
}
}
