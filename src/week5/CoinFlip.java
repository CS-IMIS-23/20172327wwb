
//***********************************************
//
//      Filename: CoinFlip.java
//
//        Author: wwb
//        Mails: 1274860621@qq.com
//        Create: 2018-04-07 13:54:10
// Last Modified: 2018-04-07 13:54:10
//***********************************************
public class CoinFlip
{
public static void main(String[] args)
{
Coin myCoin = new Coin();
myCoin.flip();
System.out.println(myCoin);
if (myCoin.isHeads())
System.out.println("You win.");
else
System.out.println("Better luck next time.");
}
}
