
//***********************************************
//
//      Filename: L51.java
//
//        Author: wwb
//        Mails: 1274860621@qq.com
//        Create: 2018-04-06 20:52:57
// Last Modified: 2018-04-06 20:52:57
//***********************************************
import java.util.Scanner;

public class Age
{
public static void main(String[]args)
{
final int MINOR = 21;
 
Scanner scan = new Scanner(System.in);
System.out.print("Enter your age: " );
int age = scan.nextInt();

System.out.println("You enter: " + age);

if (age < MINOR)
System.out.println("Youth is a wonderful thing. Enjoy");

System.out.print("Age is a state of mind.");
}
}


