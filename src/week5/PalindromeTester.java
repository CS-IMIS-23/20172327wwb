
//***********************************************
//
//      Filename: PalindromeTester.java
//
//        Author: wwb
//        Mails: 1274860621@qq.com
//        Create: 2018-04-08 15:50:47
// Last Modified: 2018-04-08 15:50:47
//***********************************************
import java.util.Scanner;

public class PalindromeTester
{
public static void main(String[]args)
{
String str , another = "y";
int left , right;

Scanner scan = new Scanner(System.in);

while (another.equalsIgnoreCase("y"))
{
System.out.println("Enter a potential palindrome: ");
str = scan.nextLine();

left = 0;
right = str.length() - 1 ;
while (str.charAt(left) == str.charAt(right) && left < right)
{
left++;
right--;
}
System.out.println();
if (left < right)
  System.out.println("That string is NOT a palindrome.");
else 
  System.out.println("That string IS a palindrome.");

System.out.println();
System.out.println("Test another paliandrome(y/n)?");
another = scan.nextLine();
}
}
}
