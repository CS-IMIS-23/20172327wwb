
//***********************************************
//
//      Filename: Calculate.java
//
//        Author: wwb
//        Mails: 1274860621@qq.com
//        Create: 2018-06-13 16:23:57
// Last Modified: 2018-06-13 16:23:57
//***********************************************
import java.util.ArrayList;
import java.util.List;

public class Calculate {
    private Mystack ms1 = new Mystack();//转化成后缀
    private Mystack ms2 = new Mystack();//运算
    private String s;
    public Calculate()
    {

    }
    public  List<String> zb(String s) {
        List<String> ls = new ArrayList<String>();//存储中序表达式，将中缀表达式转为数组形式
        int i = 0;
        String str;
        char c;
        do {
            if ((c = s.charAt(i)) < 48 || (c = s.charAt(i)) > 57) {
                ls.add("" + c);
                i++;
            } else {
                str = "";
                while (i < s.length() && (c = s.charAt(i)) >= 48
                        && (c = s.charAt(i)) <= 57) {
                    str += c;
                    i++;
                }
                ls.add(str);
            }

        } while (i < s.length());
        return ls;
    }
    public  List<String> parse(List<String> ls) {
        List<String> lss = new ArrayList<String>();
        for (String ss : ls) {
            if (ss.matches("\\d+")) {
                lss.add(ss);
            } else if (ss.equals("(")) {
                ms1.push(ss);
            } else if (ss.equals(")")) {

                while (!ms1.top.equals("(")) {
                    lss.add(ms1.pop());
                }
                ms1.pop();
            } else {
                while (ms1.size() != 0 && getValue(ms1.top) >= getValue(ss)) {
                    lss.add(ms1.pop());
                }
                ms1.push(ss);
            }
        }
        while (ms1.size() != 0) {
            lss.add(ms1.pop());
        }
        return lss;
    }
    //计算后缀表达式
    public  int suanshu(List<String> ls) {
        for (String s : ls) {
            if (s.matches("\\d+")) {
                ms2.push(s);
            } else {
                int b = Integer.parseInt(ms2.pop());
                int a = Integer.parseInt(ms2.pop());
                if (s.equals("+")) {
                    a = a + b;
                } else if (s.equals("-")) {
                    a = a - b;
                } else if (s.equals("*")) {
                    a = a * b;
                } else if (s.equals("/")) {
                    a = a / b;
                }
                ms2.push("" + a);
            }
        }
        return Integer.parseInt(ms2.pop());
    }
    /**
     * 获取运算符优先级
     * +,-为1 *,/为2 ()为0
     */
    public static int getValue(String s) {
        if (s.equals("+")) {
            return 1;
        } else if (s.equals("-")) {
            return 1;
        } else if (s.equals("*")) {
            return 2;
        } else if (s.equals("/")) {
            return 2;
        }
        return 0;
    }


}
