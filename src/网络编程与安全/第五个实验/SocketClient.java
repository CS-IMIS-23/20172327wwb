
//***********************************************
//
//      Filename: SocketClient.java
//
//        Author: wwb
//        Mails: 1274860621@qq.com
//        Create: 2018-06-17 09:07:59
// Last Modified: 2018-06-17 09:07:59
//***********************************************
import java.io.*;
import java.net.Socket;
import java.util.Scanner;

/**
 * Created by besti on 2018/6/9.
 */
public class SocketClient {
    public static void main(String[] args) throws Exception {
        //1.建立客户端Socket连接，指定服务器位置和端口
//        Socket socket = new Socket("localhost",8080);
        Socket socket = new Socket("127.0.0.1",8800);

        //2.得到socket读写流
        OutputStream outputStream = socket.getOutputStream();
 //       PrintWriter printWriter = new PrintWriter(outputStream);
        OutputStreamWriter outputStreamWriter = new OutputStreamWriter(outputStream);
        //输入流
        InputStream inputStream = socket.getInputStream();
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream,"UTF-8"));
        //3.利用流按照一定的操作，对socket进行读写操作
        Scanner scan = new Scanner(System.in);
        System.out.println("我输入的中缀表达式为: ");
        String a =scan.nextLine();
        Calculate p =new Calculate();
        String temp = "";
        for (int x = 0; x < p.parse(p.zb(a)).size(); ++x) {
            temp = temp + p.parse(p.zb(a)).get(x);
        }
        SEnc c=new SEnc();
        String ppp=c.jiami(temp);
        DigestPass oo=new DigestPass();
       String aa= oo.Diget(temp);

        String info1 = ppp+";"+aa;
        String info = new String(info1.getBytes("GBK"),"utf-8");
   //     printWriter.write(info);
   //     printWriter.flush();
        outputStreamWriter.write(info);
        outputStreamWriter.flush();
        socket.shutdownOutput();
        //接收服务器的响应
        String reply = null;
        while (!((reply = bufferedReader.readLine()) ==null)){
            System.out.println("接收服务器的信息为：" + reply);
        }
        //4.关闭资源
        bufferedReader.close();
        inputStream.close();
        outputStreamWriter.close();
        //printWriter.close();
        outputStream.close();
        socket.close();
    }
}

