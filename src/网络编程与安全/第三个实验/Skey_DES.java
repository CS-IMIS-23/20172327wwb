
//***********************************************
//
//      Filename: Skey_DES.java
//
//        Author: wwb
//        Mails: 1274860621@qq.com
//        Create: 2018-06-13 22:18:21
// Last Modified: 2018-06-13 22:18:21
//***********************************************
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;

public class Skey_DES{
    public static void main(String args[])
            throws Exception{
        KeyGenerator kg=KeyGenerator.getInstance("DESede");
        kg.init(168);
        SecretKey k=kg.generateKey( );
        System.out.println(k);
        FileOutputStream  f=new FileOutputStream("key1.dat");
        ObjectOutputStream b=new  ObjectOutputStream(f);
        b.writeObject(k);
    }
}
