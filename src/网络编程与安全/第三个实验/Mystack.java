
//***********************************************
//
//      Filename: Mystack.java
//
//        Author: wwb
//        Mails: 1274860621@qq.com
//        Create: 2018-06-13 22:16:40
// Last Modified: 2018-06-13 22:16:40
//***********************************************
import java.util.ArrayList;
import java.util.List;

public class Mystack {
    private List<String> l;
    private int size;
    public String top;

    public Mystack() {
        l = new ArrayList<String>();
        size = 0;
        top = null;
    }

    public int size() {
        return size;
    }

    public void push(String s) {
        l.add(s);
        top = s;
        size++;
    }

    public String pop() {
        String s = l.get(size - 1);
        l.remove(size - 1);
        size--;
        top = size == 0 ? null : l.get(size - 1);
        return s;
    }
}
