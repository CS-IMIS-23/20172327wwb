
//***********************************************
//
//      Filename: SocketServer.java
//
//        Author: wwb
//        Mails: 1274860621@qq.com
//        Create: 2018-06-16 21:20:53
// Last Modified: 2018-06-16 21:20:53
//***********************************************
import javax.crypto.*;
import javax.crypto.spec.SecretKeySpec;
import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.util.Arrays;
import java.util.List;

/**
 * Created by besti on 2018/6/9.
 */
public class SocketServer {
    public static void main(String[] args) throws IOException, InvalidKeyException, NoSuchPaddingException, NoSuchAlgorithmException, BadPaddingException, IllegalBlockSizeException, ClassNotFoundException {
        //1.建立一个服务器Socket(ServerSocket)绑定指定端口
        ServerSocket serverSocket=new ServerSocket(8800);
        //2.使用accept()方法阻止等待监听，获得新连接
        Socket socket=serverSocket.accept();
        //3.获得输入流
        InputStream inputStream=socket.getInputStream();
        BufferedReader bufferedReader=new BufferedReader(new InputStreamReader(inputStream,"UTF-8"));
        //获得输出流
        OutputStream outputStream=socket.getOutputStream();
        PrintWriter printWriter=new PrintWriter(outputStream);
        //4.读取用户输入信息
        String info=null;
        String str1="";
        while(!((info = bufferedReader.readLine()) ==null)){
            System.out.println("我是服务器，用户信息为：" + info);
            str1=info;}
        //给客户一个响应
            //解密总
         //  byte[] ctext=str1.getBytes();
//        FileInputStream f=new FileInputStream("SEnc.dat");
//        int num=f.available();
//        byte[ ] ctext=new byte[num];
//        f.read(ctext);
        String []o=str1.split(",");
        byte []ctext=new byte[o.length];
        for (int q =0;q<o.length;q++){
            ctext[q]= Byte.parseByte(o[q]);
        }
            // 获取密钥
//            FileInputStream  f2=new FileInputStream("keykb1.dat");
////            int num2=f2.available();
////            byte[ ] keykb=new byte[num2];
////            f2.read(keykb);
        FileInputStream f1=new FileInputStream("Apub.dat");
        ObjectInputStream b1=new ObjectInputStream(f1);
        PublicKey pbk=(PublicKey)b1.readObject( );
//读取自己的DH私钥
        FileInputStream f2=new FileInputStream("Bpri.dat");
        ObjectInputStream b2=new ObjectInputStream(f2);
        PrivateKey prk=(PrivateKey)b2.readObject( );
        // 执行密钥协定
        KeyAgreement ka=KeyAgreement.getInstance("DH");
        ka.init(prk);
        ka.doPhase(pbk,true);
        //生成共享信息
        byte[ ] sb=ka.generateSecret();
//        for(int i=0;i<sb.length;i++){
//            System.out.print(sb[i]+",");
//        }

            SecretKeySpec k=new  SecretKeySpec(sb,0,24,"DESede");
            // 解密
            Cipher cp=Cipher.getInstance("DESede");
            cp.init(Cipher.DECRYPT_MODE, k);
            byte []ptext=cp.doFinal(ctext);
            // 显示明文
            String p=new String(ptext,"UTF8");
           // System.out.println(p);
        Calculate a = new Calculate();
        String str=p;
            String[] temp = str.split(" ");
//        String[]b = new String[info.length()];
//        for (int i=0;i<info.length();i++){
//            b[i]= String.valueOf(info.charAt(i));
//        }
        List<String> ls= Arrays.asList(temp);
        String reply=String.valueOf(a.suanshu(ls));
        printWriter.write(reply);
        printWriter.flush();
        //5.关闭资源
        printWriter.close();
        outputStream.close();
        bufferedReader.close();
        inputStream.close();
        socket.close();
        serverSocket.close();
    }
}

