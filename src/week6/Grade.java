
//***********************************************
//
//      Filename: Grade.java
//
//        Author: wwb
//        Mails: 1274860621@qq.com
//        Create: 2018-04-15 16:55:43
// Last Modified: 2018-04-15 16:55:43
//***********************************************
public class Grade {
    private  String name;
    private int lowerBound;
    public Grade(String grade,int cutoff)
    {
        name = grade;
        lowerBound = cutoff;
    }
    public String toString()
    {
        return name + "\t" + lowerBound;
    }
    public void setName(String grade)
    {
        name = grade;
    }
    public void setLowerBound(int cutoff)
    {
        lowerBound = cutoff;
    }
    public String getName()
    {
        return name;
    }
    public int getLowerBound()
    {
        return lowerBound;
    }
}
