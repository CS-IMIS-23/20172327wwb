
//***********************************************
//
//      Filename: DVDCollection.java
//
//        Author: wwb
//        Mails: 1274860621@qq.com
//        Create: 2018-04-15 16:54:58
// Last Modified: 2018-04-15 16:54:58
//***********************************************
import java.text.DateFormat;
import java.text.NumberFormat;

public class DVDCollection {
    private DVD[] collection;
    private int count;
    private double totalCost;

    public DVDCollection() {
        collection = new DVD[100];
        count = 0;
        totalCost = 0.0;
    }

    public void addDVD(String title, String director, int year, double cost, boolean bluray) {
        if (count == collection.length)
            increseSize();
            collection[count] = new DVD(title, director, year, cost, bluray);
            totalCost += cost;
            count++;
    }

    public String toString() {
        NumberFormat fmt = NumberFormat.getCurrencyInstance();
        String report = "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n";
        report += "My DVD Collection\n\n";
        report += "Number of DVDs: " + fmt.format(totalCost) + "\n";
        report += "Total cost: " + fmt.format(totalCost) + "\n";
        report += "Average cost:"+ fmt.format(totalCost / count);

        report += "\n\nDVD List:\n\n";

        for (int dvd = 0; dvd < count; dvd++)
            report += collection[dvd].toString() + "\n";

        return report;
    }

    private void increseSize()
    {
        DVD[] temp = new DVD[collection.length*2];

        for (int dvd = 0;dvd<collection.length;dvd++)
            temp[dvd] = collection[dvd];

        collection = temp;
    }
}
