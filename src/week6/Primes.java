
//***********************************************
//
//      Filename: Primes.java
//
//        Author: wwb
//        Mails: 1274860621@qq.com
//        Create: 2018-04-15 16:59:24
// Last Modified: 2018-04-15 16:59:24
//***********************************************
public class Primes {
    public static void main(String[] args) {
        int[] primeNums = {2,3,5,7,11,13,17,19};

        System.out.println("Array length: " + primeNums.length);

        System.out.println("The first few prime numbers are:");

        for (int prime : primeNums)
            System.out.print(prime + " ");
    }
}
