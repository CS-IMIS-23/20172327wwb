
//***********************************************
//
//      Filename: homework2.java
//
//        Author: wwb
//        Mails: 1274860621@qq.com
//        Create: 2018-05-13 19:28:04
// Last Modified: 2018-05-13 19:28:04
//***********************************************
import java.io.*;
import java.util.Arrays;
import java.util.Scanner;
import java.util.StringTokenizer;

public class homework2 {
    public static void main(String[] args) throws IOException{
        try {
            File file = new File("D:\\test", "Test.txt");
            if (!file.exists()) {
                file.createNewFile();
            }
            String a;
            Scanner scan = new Scanner(System.in);
            System.out.println("输入几个数字： ");
            a = scan.nextLine();
            String[] sourceStrArray = a.split(" ");
       /* for (int i = 0; i < sourceStrArray.length; i++) {
            System.out.println(sourceStrArray[i]);
        }*/

            Writer writer = new FileWriter(file);
            writer.write(a);
            writer.flush();

            Reader reader = new FileReader(file);
            System.out.println();
            while (reader.ready()) {
                System.out.print((char) reader.read() + "");
            }
            String str = a.replaceAll(" ", "");
            String[] b = new String[sourceStrArray.length];

            int in = 0;
            while (in <= sourceStrArray.length - 1) {
                b[in] = sourceStrArray[in];
                in++;
            }
            System.out.println();
            int[] t = new int[b.length];
            for (int i = 0; i < b.length; i++) {
                t[i] = Integer.parseInt(b[i]);
            }

            insertionSort(t);

            System.out.println("排序后：" + Arrays.toString(t));

            FileWriter fw = new FileWriter("D:\\test\\Test.txt");
            String q = "";
            for (int i = 0; i < t.length; i++) {
                q += t[i] + ",";
            }
            fw.write(q);
            fw.flush();
            fw.close();
        }
        catch (Exception e)
        {
            System.out.print("this is a problem.");
        }

    }

    private static void insertionSort(int[] arr) {
        for (int i = 1; i < arr.length; i++) {
            int temp = arr[i];
            int j = i - 1;
            while (j >= 0 && temp < arr[j]) {
                arr[j + 1] = arr[j];
                j--;
            }
            arr[j + 1] = (char) temp;
        }

    }
}
