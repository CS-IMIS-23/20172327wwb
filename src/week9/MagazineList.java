
//***********************************************
//
//      Filename: MagazineList.java
//
//        Author: wwb
//        Mails: 1274860621@qq.com
//        Create: 2018-05-15 21:59:04
// Last Modified: 2018-05-15 21:59:04
//***********************************************
import practive.Magazine;

public class MagazineList
{
   private MagazineNode list;


   //----------------------------------------------------------------
   //  Sets up an initially empty list of magazines.
   //----------------------------------------------------------------
   public MagazineList()
   {
      list = null;
   }

   //----------------------------------------------------------------
   //  Creates a new MagazineNode object and adds it to the end of
   //  the linked list.
   //----------------------------------------------------------------
   public void add(Magazine mag)
   {
      MagazineNode node = new MagazineNode(mag);
      MagazineNode current;

      if (list == null)
      list = node;

      else
      {
         current = list;
         while (current.next != null)
          current = current.next;
         current.next = node;

      }
   }
   public  void insert(int index, Magazine newMagazine)//在index的位置插入新节点newMagazine
   {

      MagazineNode node = list;
      int j=0;
      while (node!=null&&j<index-2){//查找到第index-1个元素
         node=node.next;
         j++;
      }
      MagazineNode current = new MagazineNode(newMagazine);
      current.next=node.next;//被插入的结点
      node.next=current;
      if (index==1)
      {
          Magazine num = list.magazine;
          list.magazine=list.next.magazine;
          list.next.magazine=num;//此处和小赵同志商量着完成 将第一位和第二位进行交换
      }
      }

      public void delete(Magazine delNode)//删除节点delNode
    {

        MagazineNode node=list;
        while (!node.magazine.equals(delNode))//!node.next.magazine.equals(delNode)
        {
            node=node.next;
        }
            node.next=node.next.next;//删除寻找到的节点
        }

//参考https://blog.csdn.net/gg543012991/article/details/51030329
    //----------------------------------------------------------------
   //  Returns this list of magazines as a string.
   //----------------------------------------------------------------
   public String toString()
   {
      String result = "";

      MagazineNode current = list;

      while (current != null)
      {
         result += current.magazine + "\n";
         current = current.next;
      }

      return result;
   }

   //*****************************************************************
   //  An inner class that represents a node in the magazine list.
   //  The public variables are accessed by the MagazineList class.
   //*****************************************************************
   private class MagazineNode
   {
      public Magazine magazine;
      public MagazineNode next;

      //--------------------------------------------------------------
      //  Sets up the node
      //--------------------------------------------------------------
      public MagazineNode(Magazine mag)
      {
         magazine = mag;
         next = null;
      }
   }
}

