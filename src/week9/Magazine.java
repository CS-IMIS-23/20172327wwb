
//***********************************************
//
//      Filename: Magazine.java
//
//        Author: wwb
//        Mails: 1274860621@qq.com
//        Create: 2018-05-15 22:01:40
// Last Modified: 2018-05-15 22:01:40
//***********************************************
public class Magazine 
{
   private String title;

   //-----------------------------------------------------------------
   //  Sets up the new magazine with its title.
   //-----------------------------------------------------------------
   public Magazine(String newTitle)
   {    
      title = newTitle;
   }

   //-----------------------------------------------------------------
   //  Returns this magazine as a string.
   //-----------------------------------------------------------------
   public String toString()
   {
      return title;
   }
}

