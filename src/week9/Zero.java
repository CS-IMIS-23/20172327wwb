
//***********************************************
//
//      Filename: Zero.java
//
//        Author: wwb
//        Mails: 1274860621@qq.com
//        Create: 2018-05-13 19:38:32
// Last Modified: 2018-05-13 19:38:32
//***********************************************
public class Zero
{
   //-----------------------------------------------------------------
   //  Deliberately divides by zero to produce an exception.
   //-----------------------------------------------------------------
   public static void main(String[] args)
   {
      int numerator = 10;
      int denominator = 0;

      System.out.println(numerator / denominator);

      System.out.println("This text will not be printed.");
   }
}
