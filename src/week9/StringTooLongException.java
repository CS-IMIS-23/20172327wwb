
//***********************************************
//
//      Filename: StringTooLongException.java
//
//        Author: wwb
//        Mails: 1274860621@qq.com
//        Create: 2018-05-13 19:31:07
// Last Modified: 2018-05-13 19:31:07
//***********************************************
public class StringTooLongException extends Exception {

    StringTooLongException(String message) {
        super(message);
    }
}

