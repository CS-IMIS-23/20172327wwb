
//***********************************************
//
//      Filename: StringTooLongExTest1.java
//
//        Author: wwb
//        Mails: 1274860621@qq.com
//        Create: 2018-05-13 19:34:58
// Last Modified: 2018-05-13 19:34:58
//***********************************************
import java.util.Scanner;

public class StringTooLongExTest1 {
    public static void main(String[] args) throws StringTooLongException{

        String b="y",message = null;
        Scanner scan = new Scanner(System.in);
        while (b.equalsIgnoreCase("y"))
        { System.out.println("请输入：  ");
            Scanner a = new Scanner(System.in);
            message+=a.nextLine();
            System.out.println("请问还要继续吗？（输入y继续；输入DONE时结束）： ");
            b=scan.nextLine();
        }

        StringTooLongException q = new StringTooLongException("字符串长度超过20");
        System.out.println("当字符串长度超过20抛出，输入的字符串长度为： "+message.length());
        if(message.length()>20)
            throw q;
        else
            System.out.println("输入的结果是： " +message);

    }
}
