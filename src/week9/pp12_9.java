
//***********************************************
//
//      Filename: pp12_9.java
//
//        Author: wwb
//        Mails: 1274860621@qq.com
//        Create: 2018-05-13 21:36:30
// Last Modified: 2018-05-13 21:36:30
//***********************************************
import java.util.Scanner;

public class pp12_9 {
    public static void main(String[] args) {

            int n ;
            System.out.println("请输入想要输出第几行？ ");
            Scanner scan = new Scanner(System.in);
            n=scan.nextInt();
            displayPascal(n);
        }

        static void displayPascal(int n) {
            showRow(getRow( n));
        }


        static void showRow(int[] row) {
            for (int i = 0; i < row.length; i++) {
                System.out.print(row[i] + " ");
            }
            System.out.println();
        }

        static int[] getRow(int row) {
            if (row == 1) {
                return new int[] { 1 };
            }
            if (row == 2) {
                return new int[] { 1, 1 };
            }

            int[] result = new int[row];
            for (int i = 0; i < result.length; i++) {
                if (i == 0 || i == result.length - 1) {
                    result[i] = 1;
                } else {
                    result[i] = getRow(row - 1)[i - 1] + getRow(row - 1)[i];
                }
            }
            return result;
        }
}
