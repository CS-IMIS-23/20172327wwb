
//***********************************************
//
//      Filename: MagazineRack1.java
//
//        Author: wwb
//        Mails: 1274860621@qq.com
//        Create: 2018-05-19 23:58:59
// Last Modified: 2018-05-19 23:58:59
//***********************************************
public class MagazineRack1
{
   //----------------------------------------------------------------
   //  Creates a MagazineList1 object, adds several magazines to the
   //  list, then prints it.
   //----------------------------------------------------------------
   public static void main(String[] args)
   {    
      MagazineList1 rack = new MagazineList1();
      Magazine a = new Magazine("123");
      Magazine b = new Magazine("666");
      rack.add(new Magazine("Time"));
      rack.add(new Magazine("Woodworking Today"));
      rack.add(new Magazine("Communications of the ACM"));
      rack.add(new Magazine("House and Garden"));
      rack.add(new Magazine("GQ"));
      rack.add(b);
      System.out.println("插入前："+rack);
      rack.insert(3,a) ;
      System.out.println("插入后："+rack);
      rack.delete(a);
      System.out.println("删除后："+rack);
      rack.Sort();
      System.out.println(rack);
   }
}
