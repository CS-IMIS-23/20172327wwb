
//***********************************************
//
//      Filename: DVDRack.java
//
//        Author: wwb
//        Mails: 1274860621@qq.com
//        Create: 2018-05-19 23:40:40
// Last Modified: 2018-05-19 23:40:40
//***********************************************
public class DVDRack {
    public static void main(String[] args) {
        DVDList rack = new DVDList();
        rack.add(new DVD("The Godfather","Francis Ford Coppola",1972,24.95,true));
        rack.add(new DVD("District 9","Neill Blomkamp",2009,19.95,false));
        rack.add(new DVD("Iron Man2","Jon Faveau",2008,15.95,false));
        rack.add(new DVD("All About Eve","Joseph Mankiewicz",1950,17.50,false));

        System.out.println(rack);
    }
}
