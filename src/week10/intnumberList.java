
//***********************************************
//
//      Filename: intnumberList.java
//
//        Author: wwb
//        Mails: 1274860621@qq.com
//        Create: 2018-05-19 23:43:25
// Last Modified: 2018-05-19 23:43:25
//***********************************************
public class intnumberList {
    private intnumberNode list;
    private int chainlength=0;
    public intnumberList()
    {
        list=null;
    }
public void add(intnum num)
{
    intnumberNode node=new intnumberNode(num);
    intnumberNode current;
    if (list==null) {
        list=node;
        chainlength++;
    } else
    {
        current=list;
        while (current.next!=null) {
            current=current.next;
        }
        current.next=node;
        chainlength++;
    }
}
@Override
public String toString()
{
    String result="";
    intnumberNode current=list;
    while (current!=null)
    {
        result+=current.number+"\n";
        current=current.next;
    }
    return result;
}
public void selectsort(){
        for (intnumberNode ps = list;ps!=null&&ps.next!=null;ps=ps.next)
            {
                if (ps.number.getnewnum() > ps.next.number.getnewnum()){
                    intnum temp=ps.number;
                    ps.number=ps.next.number;
                    ps.next.number=temp;
                    }

            }
            intnumberNode ps = list;
            intnum temp=ps.number;
            ps.number=ps.next.number;
            ps.next.number=temp;

    }
public void sort(){
        intnumberNode p=list;
        for (intnumberNode ps = list;ps!=null&&ps.next!=null;ps=ps.next)
        {
           for (p.number=ps.next.number;p!=null;p=p.next)
           {
               if (ps.number.getnewnum()>p.number.getnewnum())
               {intnum temp = p.number;
               p.number=ps.number;
               ps.number=temp;
               }

           }
        }
}

private static class intnumberNode
{
    public intnum number;
    public intnumberNode next;

    public intnumberNode(intnum num)
    {
        number=num;
        next=null;
    }
}
}
