
//***********************************************
//
//      Filename: intnum.java
//
//        Author: wwb
//        Mails: 1274860621@qq.com
//        Create: 2018-05-19 23:42:15
// Last Modified: 2018-05-19 23:42:15
//***********************************************
public class intnum {
    private int num;

    public intnum(int newnum)
    {
        num =newnum;
    }
    public int getnewnum(){
        return this.num;
    }
    @Override
    public String toString()
    {
        return String.valueOf(num);
    }
}
