package week4;

class Die {

    private int faceValue;

    Die() {
        faceValue = 1;
    }

    public int roll() {
        int MAX = 6;
        faceValue = (int) (Math.random() * MAX) + 1;
        return faceValue;
    }

    public void setFaceValue(int value) {
        faceValue = value;
    }

    public int getFacevalue() {
        return faceValue;
    }

    public String toString() {
        return Integer.toString(faceValue);
    }

    public int getFaceValue() {
        return faceValue;
    }
}

