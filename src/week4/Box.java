
//***********************************************
//
//      Filename: Box.java
//
//        Author: wwb
//        Mails: 1274860621@qq.com
//        Create: 2018-04-03 17:42:56
// Last Modified: 2018-04-03 17:42:56
//***********************************************
public class Box {
    private double high;
    private double thickness;
    private double width;
    private boolean full;


    public  Box(double High, double Thickness,double Width){
        high = High;
        thickness = Thickness;
        width = Width;
        full = false;

    }

    public double getHigh() {
        return getHigh();
    }
    public void setHigh(double High){
        high = High;
    }
    public double getThickness(){
        return getThickness();
    }
    public void setThickness(double Thickness){
    thickness = Thickness;
}
    public double getWidth(){
        return getWidth();
    }
    public void setWidth(double Width) {
        width = Width;
    }
    public boolean Full(){
       ;
       return full;
    }
    public String toString(){
        return "\nhigh: " + high + "\nthickness" + thickness + "\nwidth ; " + width ;
    }
}
