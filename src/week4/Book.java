
//***********************************************
//
//      Filename: Book.java
//
//        Author: wwb
//        Mails: 1274860621@qq.com
//        Create: 2018-03-28 11:15:13
// Last Modified: 2018-03-28 11:15:13
//***********************************************
  public class Book
{
	private String bookname;
        private String author;
	private String press;
	private String copyrightdate;
	
public Book(String bk , String au , String pr , String crd)
{
    bookname = bk;
    author = au;
    press = pr;
    copyrightdate = crd;
} 
 public String getbookname()
  { 
 return bookname;
}
public void setbookname(String bookname){
bookname = bookname;}

 public String getauthor(){
return author;
}
public void setauthor(String author){
author = author;}

  public String getpress()
{
 return press;
}
public void setpress(String press){
press = press;}

   public String getcopyrightdate()
{
return  copyrightdate;
}
public void setcopyrightdate(String copyrightdate){
copyrightdate = copyrightdate;}

public String toString()
{ 
return"\nBookname: " + bookname  +" \nAuthor: " + author + "\nPress: " +  press + "\nCopyrightdate: " +  copyrightdate;}
}
    
