
//***********************************************
//
//      Filename: Book.java
//
//        Author: wwb
//        Mails: 1274860621@qq.com
//        Create: 2018-04-21 11:44:16
// Last Modified: 2018-04-21 11:44:16
//***********************************************
public class Book {
    protected int pages = 1500;
    public void setPages(int numPages)
    {
        pages = numPages;
    }
    public int getPages()
    {
        return pages;
    }

}
