
//***********************************************
//
//      Filename: Sheep.java
//
//        Author: wwb
//        Mails: 1274860621@qq.com
//        Create: 2018-04-23 09:25:51
// Last Modified: 2018-04-23 09:25:51
//***********************************************
public class Sheep extends Animal {
    public Sheep(String name, int id) {
        super(name, id);
    }

    @Override
    public void eat() {
       System.out.println(name+" wanna to eat ");
    }

    @Override
    public void sleep() {
        System.out.println(name+" want to sleep");

    }

    @Override
    public void introduction() {
       System.out.println(name+"my name is "+"my id " +id);
    }
}
