
//***********************************************
//
//      Filename: MyComplexTest.java
//
//        Author: wwb
//        Mails: 1274860621@qq.com
//        Create: 2018-04-21 15:38:21
// Last Modified: 2018-04-21 15:38:21
//***********************************************
import org.junit.Test;
import junit.framework.TestCase;

public class MyComplexTest extends TestCase {
    MyComplex a = new MyComplex(1, 2);
    MyComplex b = new MyComplex(2, 3);
    MyComplex c = new MyComplex(3, 5);
    MyComplex d = new MyComplex(1, 1);
    MyComplex e = new MyComplex(-4,7);
    MyComplex f = new MyComplex(1,2);
    MyComplex g = new MyComplex(1,0);
    @Test
    public void testComplexAdd() throws Exception {
        assertEquals(String.valueOf(c), String.valueOf(a.ComplexAdd(b)));
        }
    @Test
    public void testComplexSub() throws Exception{
        assertEquals(String.valueOf(d),String.valueOf(b.ComplexSub(a)));
    }
    @Test
    public void testComplexMultib() throws Exception{
        assertEquals(String.valueOf(e),String.valueOf( a.ComplexMulti(b)));
    }
    @Test
    public void testComplexDiv() throws Exception{
        assertEquals(String.valueOf(g),String.valueOf(f.ComplexDiv(a)));
    }
}
