
//***********************************************
//
//      Filename: Coin.java
//
//        Author: wwb
//        Mails: 1274860621@qq.com
//        Create: 2018-04-21 11:55:01
// Last Modified: 2018-04-21 11:55:01
//***********************************************
public class Coin {
    private final int HEADS = 0;
    private final int TAILS = 1;
    protected int face;
    public Coin()
    {
        flip();
    }
    public void flip()
    {
        face = (int)(Math.random() * 2);

    }

    public boolean isHeads()
    {
        return (face == HEADS);
    }
    public String toString()
    {
        String faceName;
        if (face == HEADS)
            faceName = "Heads";
        else
            faceName = "Tails";
        return faceName;
    }
}
