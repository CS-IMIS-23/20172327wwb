
//***********************************************
//
//      Filename: Advice.java
//
//        Author: wwb
//        Mails: 1274860621@qq.com
//        Create: 2018-04-21 11:41:05
// Last Modified: 2018-04-21 11:41:05
//***********************************************
public class Advice extends Thought {
    public void message() {
        System.out.println("Warning: Dates in calendar are closer " + "than they appear ");
        System.out.println();

        super.message();
    }
}
