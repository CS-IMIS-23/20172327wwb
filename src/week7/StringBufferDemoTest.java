
//***********************************************
//
//      Filename: StringBufferDemoTest.java
//
//        Author: wwb
//        Mails: 1274860621@qq.com
//        Create: 2018-04-21 12:02:09
// Last Modified: 2018-04-21 12:02:09
//***********************************************
import org.junit.Test;
import junit.framework.TestCase;

public class StringBufferDemoTest extends TestCase {
  StringBuffer a = new StringBuffer("Stirngbutter");
  StringBuffer b = new StringBuffer("naSsfjodgnan");
  StringBuffer c  = new StringBuffer("fakofnm");

  @Test
    public void testcharAt() throws Exception
  {
      assertEquals('S',a.charAt(0));

  }
  @Test
    public void testcapacity() throws Exception
  {
      assertEquals(28,a.capacity());
      assertEquals(28,a.capacity());
      assertEquals(23,c.capacity());
  }
  @Test
    public void testlength() throws Exception
  {
      assertEquals(12,a.length());
      assertEquals(12,b.length());
      assertEquals(7,c.length());
  }

}
