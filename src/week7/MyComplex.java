
//***********************************************
//
//      Filename: MyComplex.java
//
//        Author: wwb
//        Mails: 1274860621@qq.com
//        Create: 2018-04-21 15:37:27
// Last Modified: 2018-04-21 15:37:27
//***********************************************
import java.util.Scanner;
public class MyComplex {
    double RealPart;
    double ImagePart;

    MyComplex() {
        Scanner input = new Scanner(System.in);
        double RealPart = input.nextDouble();
        double ImagePart = input.nextDouble();
        new MyComplex(RealPart, ImagePart);
    }

    public MyComplex(double realPart, double imagePart) {
        RealPart = realPart;
        ImagePart = imagePart;
    }

    public void setRealPart(double realPart) {
        RealPart = RealPart;
    }

    public double getRealPart() {
        return RealPart;
    }

    public void setImagePart(double imagePart) {
        ImagePart = ImagePart;
    }

    public double getImagePart() {
        return ImagePart;
    }

    MyComplex ComplexAdd(MyComplex a) { // 复数相加
        double real2 = a.getRealPart();
        double image2 = a.getImagePart();
        double newReal = RealPart + real2;
        double newImage = ImagePart + image2;
        MyComplex result = new MyComplex(newReal, newImage);
        return result;
    }

    MyComplex ComplexSub(MyComplex a) { // 复数相减
        double real2 = a.getRealPart();
        double image2 = a.getImagePart();
        double newReal = RealPart - real2;
        double newImage = ImagePart - image2;
        MyComplex result = new MyComplex(newReal, newImage);
        return result;
    }

    MyComplex ComplexMulti(MyComplex a) { // 复数相乘
        double real2 = a.getRealPart();
        double image2 = a.getImagePart();
        double newReal = RealPart * real2 - ImagePart * image2;
        double newImage = ImagePart * real2 + RealPart * image2;
        MyComplex result = new MyComplex(newReal, newImage);
        return result;
    }

    MyComplex ComplexDiv(MyComplex a) { // 复数相除
        double real2 = a.getRealPart();
        double image2 = a.getImagePart();
        double newReal = (RealPart * real2 + ImagePart * image2) / (real2 * real2 + image2 * image2);
        double newImage = (ImagePart * real2 - RealPart * image2) / (real2 * real2 + image2 * image2);
        MyComplex result = new MyComplex(newReal, newImage);
        return result;
    }


    public String toString() { // 输出
        if (ImagePart > 0) {
            return (RealPart + " + " + ImagePart + "i");
        } else if (ImagePart < 0) {
            return (RealPart + "" + ImagePart + "i");
        } else {
            return String.valueOf(RealPart);
        }
    }
}
