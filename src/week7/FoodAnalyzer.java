
//***********************************************
//
//      Filename: FoodAnalyzer.java
//
//        Author: wwb
//        Mails: 1274860621@qq.com
//        Create: 2018-04-21 11:51:39
// Last Modified: 2018-04-21 11:51:39
//***********************************************
public class FoodAnalyzer {
    public static void main(String[] args) {
        Pizza special = new Pizza(275);
        System.out.println("Calories per serving: " + special.caloriesPerServing());
    }
}
