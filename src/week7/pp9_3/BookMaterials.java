
//***********************************************
//
//      Filename: BookMaterials.java
//
//        Author: wwb
//        Mails: 1274860621@qq.com
//        Create: 2018-04-22 20:40:34
// Last Modified: 2018-04-22 20:40:34
//***********************************************
public class BookMaterials {
    private String name;
    private int pages;
    private String keyword;

public BookMaterials(String Name, int Pages, String Keyword)
{
    name = Name;
    pages = Pages;
    keyword = Keyword;
}
public void setName(String bookname)
{
    name = bookname;
}
public String getName()
{
    return name;
}
public void getPages(int bookpages)
{
    pages = bookpages;
}
public int getPages()
{
    return pages;
}
public void setPages(String bookkeyword)
{
    keyword = bookkeyword;
}
public String getKeyword()
{
    return keyword;
}

}
