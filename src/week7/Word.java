
//***********************************************
//
//      Filename: Word.java
//
//        Author: wwb
//        Mails: 1274860621@qq.com
//        Create: 2018-04-21 11:50:05
// Last Modified: 2018-04-21 11:50:05
//***********************************************
public class Word{
public static void main(String[] args) {
    Dictionary webster = new Dictionary();
    System.out.println("Number of pages: " + webster.getPages());
    System.out.println("Number of definitions: " + webster.getDefinitions());
    System.out.println("Definitions per page: " + webster.computeRatio());
    }
}
