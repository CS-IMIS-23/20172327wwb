
//***********************************************
//
//      Filename: FoodItem.java
//
//        Author: wwb
//        Mails: 1274860621@qq.com
//        Create: 2018-04-21 11:52:05
// Last Modified: 2018-04-21 11:52:05
//***********************************************
public class FoodItem {
    final private int CALORIES_PER_GRAM = 9;
    private  int fatGrams;
    protected int servings;
    public FoodItem(int numFatGrams,int numServings )
    {
        fatGrams = numFatGrams;
        servings = numServings;
    }
    private int caLories()
    {
        return fatGrams * CALORIES_PER_GRAM;
    }
    public int caloriesPerServing()
    {
        return (caLories()/servings);
    }

}
