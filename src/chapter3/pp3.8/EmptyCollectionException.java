package chapter3.pp3_8;

public class EmptyCollectionException extends RuntimeException {
    public EmptyCollectionException(String collection){
        super("The " + collection +" is empty.");
    }
}
