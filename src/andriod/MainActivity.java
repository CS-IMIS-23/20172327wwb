
//***********************************************
//
//      Filename: MainActivity.java
//
//        Author: wwb
//        Mails: 1274860621@qq.com
//        Create: 2018-05-28 16:09:10
// Last Modified: 2018-05-28 16:09:10
//***********************************************
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Stack;

public class MainActivity extends AppCompatActivity {
    Stack <String>a = new Stack();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toast toast=Toast.makeText(this,"TEST",Toast.LENGTH_LONG);
        Button button1=(Button)findViewById(R.id.button1);
        Button button2=(Button)findViewById(R.id.button2);
        button1.setOnClickListener(new mybuttoninsert1());
        button2.setOnClickListener(new mybuttoninsert2());
    }

    public class mybuttoninsert1 implements View.OnClickListener {
        @Override
        public void onClick(View view) {
            EditText b = (EditText) findViewById(R.id.editText1);
            b.getText();
            a.push(b.getText().toString());
            System.out.println(b.getText().toString());
            EditText c = (EditText) findViewById(R.id.editText5);
            c.setText(b.getText(), TextView.BufferType.EDITABLE);
            c.setText(a.toString(), TextView.BufferType.EDITABLE);

        }
    }
        public class mybuttoninsert2 implements View.OnClickListener{
            @Override
            public void onClick(View view) {
                a.pop();
                EditText d =(EditText)findViewById(R.id.editText5);
                d.setText(a.toString(),TextView.BufferType.EDITABLE);
            }
        }
    }

