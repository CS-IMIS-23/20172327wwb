package chapter15.pp15_1;

import chapter15.pp4_2.EmptyCollectionException;
import chapter15.pp4_2.LinnearNode;
import chapter15.pp4_2.StackADT;

public class LindedStack<T> implements StackADT<T> {
    private int count;
    private LinnearNode<T> top;
    public LindedStack(){
    count=0;
    top=null;
    }

    @Override
    public void push(T element) {
        LinnearNode<T> temp=new LinnearNode<T>(element);

        temp.setNext(top);
        top = temp;
        count++;
    }

    @Override
    public T pop() throws EmptyCollectionException {

        if (isEmpty())
            throw new EmptyCollectionException("Stack");

        T result = top.getElement();
        top=top.getNext();
        count--;

        return result;
    }

    @Override
    public T peek() {

        if (isEmpty())
            throw new EmptyCollectionException("Stack");
        T result = top.getElement();

        top=top.getNext();

       // System.out.println(result);
        return result;
    }

    @Override
    public boolean isEmpty() {

        boolean p ;
        if (size()==0){
            p=true;
        }
        else
        {
            p=false;
        }
        return p;
    }

    @Override
    public int size() {


        return count;
    }
    @Override
    public String toString(){
        LinnearNode node = new LinnearNode();
        String result="";
        node=top;
        int a =count;
        while (a>0) {

            result+=node.getElement()+" ";

            node = node.getNext();
            a--;
        }

       // System.out.println(result);
        return result;
    }

    public static void main(String[] args) {
       // LinnearNode<Integer> b = new LinnearNode<Integer>();


        LindedStack<Integer> a = new LindedStack<Integer>();
        a.push(8);
        a.push(1);
        a.push(2);
        a.push(3);
        System.out.println("连续弹出的数字");
        System.out.println(a.pop());
        System.out.println(a.pop());
        System.out.println("弹出后为：");
        System.out.println(a.toString());
//        System.out.println("输出后：");
//        System.out.println(a.toString());
        System.out.println("长度为：");
        System.out.println(a.size());
        System.out.println("栈顶为：");
        System.out.println(a.peek());
        System.out.println("是否为空：");
        System.out.println(a.isEmpty());

    }
}
