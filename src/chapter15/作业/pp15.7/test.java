package chapter15.pp15_8;

public class test {
    public static void main(String[] args) {
        Network a =new Network();
        a.addVertex("A");
        a.addVertex("B");
        a.addVertex("C");
        a.addVertex("D");
        a.addEdge("A","B",140);
        a.addEdge("A","C",100);
       a.addEdge("A","D",200);
       a.addEdge("B","C",100);
       a.addEdge("B","D",10);
       a.addEdge("C","D",300);
        System.out.println(a.toString());
//        a.removeVertex(2);
//        System.out.println(a.toString());
        System.out.println("从A到D的最短路径为"+a.shortestPathLength("A","D"));
        System.out.println();
//        Iterator b =a.iteratorBFS("A");
//        while (b.hasNext()){
//            System.out.println(b.next());
//        }
        System.out.println("是否为连通的：");
        System.out.println(a.isConnected());
      //  System.out.println(a.shortestPathWeight("A","D"));
        System.out.println("从A到D的最小权重（最小钱）"+a.shortestPathWeight("A","D"));
    }
}
