
//***********************************************
//
//      Filename: Book.java
//
//        Author: wwb
//        Mails: 1274860621@qq.com
//        Create: 2018-03-29 20:22:29
// Last Modified: 2018-03-29 20:22:29
//***********************************************
public class Book
{
	private String Bookname;
        private String Author;
	private String Press;
	private String Copyrightdate;
	
public Book(String bookname , String author , String press , String copyrightdate)
{
    Bookname = bookname;
    Author = author;
    Press = press;
    Copyrightdate = copyrightdate;
} 
 public String getBookname()
  { 
 return Bookname;
}
public void setBookname(String bookname){
Bookname = bookname;}

 public String getAuthor(){
return Author;
}
public void setAuthor(String author){
Author = author;}

  public String getPress()
{
 return Press;
}
public void setPress(String press){
Press = press;}

   public String getCopyrightdate()
{
return  Copyrightdate;
}
public void setCopyrightdate(String copyrightdate){
Copyrightdate = copyrightdate;}

public String toString()
{ 
return"\nBookname: " + Bookname  +" \nAuthor: " + Author + "\nPress: " +  Press + "\nCopyrightdate: " +  Copyrightdate;}
}
