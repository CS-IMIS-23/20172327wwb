package chapter7.作业.pp9_2;



public class pp9_2 {
    public static <T extends Comparable<T>>
    void bubbleSortTRA(T[] date,int i) {
        for (int a =i;a>=1;a=a-2){
            for (int po=date.length-1;po>=0;po--) {
                for (int scan = 0; scan < date.length - a; scan++) {
                    if (date[scan].compareTo(date[scan + a]) > 0) {
                        swap(date, scan, scan + a);
                    }
                }
            }
        }
    }


    private static <T extends Comparable<T>>
    void swap(T[] data, int index1, int index2)
    {
        T temp = data[index1];
        data[index1] = data[index2];
        data[index2] = temp;
    }
}
