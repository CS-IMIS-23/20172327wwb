package 第三次实验;

import junit.framework.TestCase;
import org.junit.Test;
import 第三次实验.cn.edu.besti.cs1723.WWB2329.Searching;
import 第三次实验.cn.edu.besti.cs1723.WWB2329.Sorting;

public class testTest extends TestCase {

    Searching a =new Searching();
    Sorting b =new Sorting();


     @Test
    public void testsetA() {
         Integer[] temp = new Integer[]{1, 4, 3, 8, 5, 9, 6, 2, 7};
         assertEquals(true, a.linearSearch(temp, 0, 8, 8));//正常情况
         assertEquals(true, a.linearSearch(temp, 0, 8, 5));//正常情况
         assertEquals(true, a.linearSearch(temp, 0, 8, 1));//边界情况
         assertEquals(true, a.linearSearch(temp, 0, 8, 7));//边界情况
//        assertEquals("[1, 2, 3, 4, 5, 6, 7, 8, 9]",String.valueOf(b.selectionSort(temp)));
//        assertEquals(false,String.valueOf(b.selectionSort(temp2)));
     }
         @Test
         public void testsetB(){
         String[] temp2=new String[]{"Q","F","W","E","T","G"};
         assertEquals(true,a.linearSearch(temp2,0,5,"Q"));//正常情况
         assertEquals(true,a.linearSearch(temp2,0,5,"W"));//正常情况
         assertEquals(true,a.linearSearch(temp2,0,5,"Q"));//边界情况
         assertEquals(true,a.linearSearch(temp2,0,5,   "G"));//边界情况


}
    @Test
    public void testsetC(){
        Integer[] temp = new Integer[]{65,345,635,25,7653,235,763525,345};
        assertEquals(true,a.linearSearch(temp,0,7,65));//正常情况
        assertEquals(true,a.linearSearch(temp,0,7,635));//正常情况
        assertEquals(false,a.linearSearch(temp,0,7,1));//边界情况
        assertEquals(false,a.linearSearch(temp,0,7,1));//边界情况
    }
    @Test
    public void testsetD(){
         String[] temp=new String[]{"A","M","A","Z","I","N","G"};
         assertEquals(true,a.linearSearch(temp,0,6,"A"));//正常情况
         assertEquals(true,a.linearSearch(temp,0,6,"G"));//边界情况
         assertEquals(true,a.linearSearch(temp,0,6,"Z"));//边界情况
         assertEquals(false,a.linearSearch(temp,0,6,"P"));//正常情况
    }
    @Test
    public void testsetE() {
        String[] temp=new String[]{"E",null,"s","P"};
        assertEquals(false,a.linearSearch(temp,0,3,"的"));//正常情况
        assertEquals(true,a.linearSearch(temp,0,3,"E"));//边界情况
        assertEquals(true,a.linearSearch(temp,0,3,"P"));//边界情况
        assertEquals(false,a.linearSearch(temp,0,3,"S"));//正常情况

    }
    @Test
    public void testsetF() {
        Integer[] temp=new Integer[]{2,0,1,7,2,3,2,9};
        assertEquals(true,a.linearSearch(temp,0,3,3));//正常情况
        assertEquals(true,a.linearSearch(temp,0,3,2));//边界情况
        assertEquals(true,a.linearSearch(temp,0,3,9));//边界情况
        assertEquals(false,a.linearSearch(temp,0,3,3));//正常情况

    }

    @Test
    public void testsetG() {
        Integer[] temp=new Integer[]{2,0,1,7,2,3,2,9};
        assertEquals(false,a.linearSearch(temp,0,3,3));//正常情况
        assertEquals(true,a.linearSearch(temp,0,3,2));//边界情况
        assertEquals(false,a.linearSearch(temp,0,3,9));//边界情况
        assertEquals(false,a.linearSearch(temp,0,3,3));//正常情况

    }
    @Test
    public void testsetH() {
        Integer[] temp=new Integer[]{2,0,1,7,2,3,2,9};
        assertEquals("[0, 1, 2, 2, 2, 3, 7, 9]",b.selectionSort(temp));//正序情况

    }
    @Test
    public void testsetI() {
        Integer[] temp=new Integer[]{9,2,3,2,7,1,0,2};
        assertEquals("[0, 1, 2, 2, 2, 3, 7, 9]",b.selectionSort(temp));//反序情况

    }
    @Test
    public void testsetJ() {
        Integer[] temp=new Integer[]{5,2,0,1,3,1,4};
        assertEquals("[0, 1, 1, 2, 3, 4, 5]",b.selectionSort(temp));//正序情况

    }
    @Test
    public void testsetK() {
        Integer[] temp=new Integer[]{4,1,3,1,0,2,5};
        assertEquals("[0, 1, 1, 2, 3, 4, 5]",b.selectionSort(temp));//反序情况

    }
    @Test
    public void testset1() {
        Integer[] temp=new Integer[]{4,1,3,1,0,2,5};
        assertEquals("[5, 4, 3, 2, 1, 1, 0]",b.selectionSort1(temp));//反序情况 //降序

    }
    @Test
    public void testset2() {
        String[] temp=new String[]{"A","B"};
        assertEquals("[B, A]",b.selectionSort1(temp));//反序情况 //降序

    }
}