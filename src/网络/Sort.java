
//***********************************************
//
//      Filename: Sort.java
//
//        Author: wwb
//        Mails: 1274860621@qq.com
//        Create: 2018-06-11 11:15:32
// Last Modified: 2018-06-11 11:15:32
//***********************************************
import java.util.Arrays;

public class Sort {
    public Sort(){

    }
    public String selectionSort(int[] a) {
        int n = a.length;
        for (int i = 0; i < n; i++) {
            int k = i;
            // 找出最小值的小标
            for (int j = i + 1; j < n; j++) {
                if (a[j] < a[k]) {
                    k = j;
                }
            }
            // 将最小值放到排序序列末尾
            if (k > i) {
                int tmp = a[i];
                a[i] = a[k];
                a[k] = tmp;
            }
        }
            String p ;
        for (int i : a) {
            p=i+" ";
           // System.out.print(i + " ");
        }
        return p= "排序后"+Arrays.toString(a);
    }

}

